<?php
/**
 * Created by PhpStorm.
 * User: Grzesiek
 * Date: 12.01.2018
 * Time: 12:35
 */

// added because coingate loader fails to load custom exceptions

//require_once MAINDIR.'includes/modules/Payment/coingate/vendor/coingate/coingate-php/lib/ApiError.php';
class coingate_controller extends HBController{
    use \Components\Traits\LoggerTrait;


    /**
     * @var coingate
     */
    var $module;



    /**
     * Method pay manages the pay invoices.
     * @param $params
     */
    public function pay($params) {


        if (!isset($params['invoice_id']) || !$this->authorization->get_login_status()) {
            Utilities::redirect('?cmd=root');
        }

        $client_id = $this->authorization->get_id();
        $invoice = Invoice::createInvoice($params['invoice_id']);

        if ( $invoice->getInvoiceId() != $params['invoice_id'] || $invoice->getClientId() != $client_id) {
            Utilities::redirect('?cmd=root');
        }

        $client = HBLoader::LoadModel('Clientarea');
        $this->module->setClient($client->get_client_details($client_id));
        $this->module->setInvoice($params['invoice_id']);
        $this->module->setCurrency($invoice->getCurrency());


        if($params['token_valid']) {


            $this->module->generateUrl($params);

        }
        else{
            Utilities::redirect('?cmd=root');
        }


    }
}