<?php
/**
 * Created by PhpStorm.
 * User: Grzesiek
 * Date: 12.01.2018
 * Time: 11:47
 */

require_once MAINDIR . 'includes' . DS . 'modules' . DS . 'Payment' . DS . 'coingate' . DS . 'vendor' . DS . 'autoload.php';

use CoinGate\CoinGate as CoinGateApi;

class coingate extends PaymentModule{

    use \Components\Traits\LoggerTrait;
    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'CoinGate';

    /**
     * Module version
     * @var string
     */
    protected $version = '1.1';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = "CoinGate Module for HostBill. <br><br>
        Module configuration:<br>
        1. Go to CoinGate dashboard and select <strong>API</strong> and then <strong>Apps</strong> in the side menu<br>
        2. Select the <strong>New App</strong>, fill in the empty fields and press <strong>Create</strong><br>
        3. Received <strong>App ID</strong> and <strong>API Key</strong> copy to the module in HostBill";

    /**
     * List of currencies supported by gateway - if module supports all currencies - leave empty
     * @var array
     */
    protected $supportedCurrencies = array(
        'USD', 'EUR', 'BTC', 'PLN', 'BSV', 'TRX',
        'GBP', 'SEK', 'NOK', 'DKK', 'CLP', 'TWD',
        'CHF', 'ZAR', 'AUD', 'JPY', 'DAI', 'USDS',
        'NZD', 'TRY', 'BRL', 'CAD', 'DASH', 'USDT',
        'CNY', 'HKD', 'HUF', 'INR', 'ETH', 'XRP',
        'RUB', 'ILS', 'MYR', 'MXN', 'LTC', 'ZEC',
        'SGD', 'RON', 'VEF', 'IDR', 'NANO',
        'PHP', 'ARS', 'THB', 'NGN', 'RSD',
        'COP', 'PKR', 'AED', 'UAH', 'SRN',
        'BGN', 'HRK', 'CZK', 'BCH', 'TEL',
    );

    /**
     * Module configuration, visible in Settings->modules
     * @var array
     */
    protected $configuration = array(
        'App ID' => array(
            'value' => '',
            'type' => 'input',
            'description' => 'Please provide your App ID from Coingate dashboard'
        ),
        'Auth Token' => array(
            'value' => '',
            'type' => 'input',
            'description' => 'Please provide your API Key from Coingate dashboard'
        ),
       'Receive Currency' => array(
            'value' => '',
            'type' => 'select',
            'description' => 'Currency you want to receive when making withdrawal at CoinGate',
            'default' => array('BTC', 'EUR', 'USD')
        ),
        'Test Mode' => array(
            'value' => '0',
            'type' => 'check'
        )
    );

    public function getConnectionParams(){
        if ($this->configuration['Test Mode']['value'] == 0){
            $environment = 'live';
        }else{
            $environment = 'sandbox';
        }

        $connection_params = [
            'environment'   => $environment,
            'auth_token'    => $this->configuration['Auth Token']['value'],
            'app_id'        => $this->configuration['App ID']['value'],

        ];

        return $connection_params;
    }

    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details).
     * @param bool $autosubmit
     * @return string
     */
    public function drawForm($autosubmit = false){

        $security_token = Tokenizer::getSalt();

        $this->form .= "<form id='payform' action='?cmd=coingate&action=pay' METHOD=POST style='margin-top: 20px;'>";
        $this->form .= "<input type='hidden' name='invoice_id' value='".$this->invoice_id."'>
                        <input type='hidden' name='client_id' value='".$this->client['id']."'>
                        <input type='hidden' name='security_token' value='".$security_token."'>";
        $this->form .= "<input id='paumpw' type='submit' value= '".$this->paynow_text()."' />";
        $this->form .= "</form>";


        if ($autosubmit){
            $this->form .= "<script>
                     $(document).ready(function(){
                        $('form#payform').siblings('img').remove();
                        $('form#payform .button-pay').hide();
                        $('form#payform').parent().contents().filter(function () {
                             return this.nodeType === 3;
                        }).remove();
                        $('#paumpw').click().hide();
                     });
                </script>";
        }

        return $this->form;
    }

    /**
     * Data coming in from payment gateway.
     */
    public function callback(){

        $data = @file_get_contents("php://input");
        parse_str($data, $response);

        if (isset($response['status'])){
            $tmp = explode('<#>', $response['order_id']);
            $response['client_id'] = $tmp[0];
            $response['invoice_id'] = $tmp[1];

            switch ($response['status']){
                case 'pending':
                    $this->logBuilder($response);
                    break;
                case 'confirming':
                    $this->logBuilder($response);
                    break;
                case 'paid':
                $this->addTransaction(array(
                    'client_id' => $response['client_id'],
                    'invoice_id' => $response['invoice_id'],
                    'description' => "Payment for invoice #" .$response['invoice_id'].' -> Receive amount: '.$response['receive_amount'].' '.$response['receive_currency'],
                    'transaction_id' => "CoinGate_".$response['id'],
                    'in' => $response['price_amount'],
                    'fee' => '0'
                ));
                    $this->logBuilder($response);
                    break;
                case 'invalid':
                    $this->logBuilder($response);
                    break;
                case 'expired':
                    $this->logBuilder($response);
                    break;
                case 'canceled':
                    $this->logBuilder($response);
                    break;
                case 'refunded':
                    break;
                default:
                    $this->module->logActivity(array(
                        'output' => [
                            'message' => 'Wrong response',
                            'source' => $response
                        ],
                        'result' => PaymentModule::PAYMENT_FAILURE
                    ));
                    $this->logger()->debug("Wrong response", $response);
                    break;
            }
        }
    }

    private function logBuilder($response){

        if ($response['status'] == 'confirming'){
            $message = ' - Wait for the invoice to change status';
        }
        if ($response['status'] == 'paid'){
            $result = PaymentModule::PAYMENT_SUCCESS;
            $this->addInfo('The invoice status: '.$response['status'].isset($message)?$message:"");
        }elseif ($response['status'] == 'pending' || $response['status'] == 'confirming'){
            $result = PaymentModule::PAYMENT_PENDING;
            $this->addInfo('The invoice status: '.$response['status'].isset($message)?$message:"");
        }else{
            $result = PaymentModule::PAYMENT_FAILURE;
            $this->addError('The invoice status: '.$response['status'].isset($message)?$message:"");
        }
        $this->logActivity(array(
            'output' => [
                'action' => $response['status'],
                'client' => $response['client_id'],
                'invoice' => $response['invoice_id'],
                'source' => $response
            ],
            'result' => $result
        ));
    }

    public function generateUrl($params){


        $invoice = Invoice::createInvoice($params['invoice_id']);


        $url = Utilities::url('?cmd=clientarea&action=invoice&id='.$params['invoice_id']);
        $amount = $invoice->getTotal();
        $currency = Utilities::getCurrency($invoice->getCurrency())['iso'];
        $invoice_id = $invoice->getInvoiceId();


        $invoice_params = [
            'order_id'          => $invoice->getClientId()."<#>".$invoice_id,
            'price_amount'      => $amount,
            'price_currency'    => $currency,
            'receive_currency'  => $this->configuration['Receive Currency']['value'],
            'title'             => "Invoice #".$invoice_id,
            'description'       => "",
            'callback_url'      => $this->getCallbackUrl(),
            'cancel_url'        => $url,
            'success_url'       => $url,


        ];

        try{
            $order = \CoinGate\Merchant\Order::create($invoice_params, array(), $this->getConnectionParams());
            if($order){
                Utilities::redirect($order->payment_url);
            }else{
                $this->logger()->debug('Order is not valid', $invoice_params);
            }
        }catch (\Exception $e){
            Engine::addError($e->getMessage());
            $this->logger()->error($e->getMessage(), array('Client: '.$params['client_id'].', Invoice: '.$params['invoice_id'],$e));
            $this->module->logActivity(array(
                'output' => [
                    'error' => $e->getMessage(),
                    'client' => $params['client_id'],
                    'invoice' => $params['invoice_id'],
                    'source' => $invoice_params
                ],
                'result' => PaymentModule::PAYMENT_FAILURE
            ));
            Utilities::redirect("?cmd=clientarea&action=invoices");
        }
    }
}